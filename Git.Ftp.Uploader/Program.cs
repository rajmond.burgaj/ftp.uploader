﻿using FluentFTP;

var server = Environment.GetEnvironmentVariable("ftp_server") ?? throw new ArgumentException("ftp_server variable is required to continue");
var username = Environment.GetEnvironmentVariable("ftp_username") ?? throw new ArgumentException("ftp_username variable is required to continue");
var password = Environment.GetEnvironmentVariable("ftp_password") ?? throw new ArgumentException("ftp_password variable is required to continue");
var serverDir = Environment.GetEnvironmentVariable("fto_server_dir") ?? throw new ArgumentException("fto_server_dir variable is required to continue");
var localDir = Environment.GetEnvironmentVariable("ftp_local_dir") ?? throw new ArgumentException("ftp_local_dir variable is required to continue");

//We retrieve all files here from the system
var files = Directory.GetFiles(localDir, "*.*", SearchOption.AllDirectories).ToList();

var take = files.Count / 2;

//Lets use two tasks to upload files in parallel
var tasks = files.Chunk(take).Select(fileChunk => UploadAsync(server, username, password, serverDir, fileChunk, CancellationToken.None)).ToList();

await Task.WhenAll(tasks);

Console.WriteLine("All files uploaded successfully!");

static async Task UploadAsync(
    string server, 
    string username, 
    string password, 
    string serverDir,
    IReadOnlyCollection<string> filePaths,
    CancellationToken cancellationToken
    )
{
    using var ftp = await GetConnectedClientAsync(server, username, password, cancellationToken);

    await ftp.UploadFilesAsync(
        filePaths,
        serverDir,
        FtpRemoteExists.Overwrite,
        true,
        FtpVerify.Retry | FtpVerify.Delete,
        FtpError.Throw,
        cancellationToken,
        new Progress<FtpProgress>(progress =>
        {
            Console.WriteLine($"Transferring local file: {progress.LocalPath}");
        }));

    await ftp.DisconnectAsync(cancellationToken);

    Console.WriteLine("List of files uploaded successfully!");

    foreach (var filePath in filePaths)
    {
        Console.WriteLine($"* {filePath}");
    }
}

static async Task<FtpClient> GetConnectedClientAsync(string server, string username, string password, CancellationToken cancellationToken)
{
    var ftp = new FtpClient(server, username, password);

    ftp.OnLogEvent += (level, s) =>
    {
        if (level is FtpTraceLevel.Warn or FtpTraceLevel.Error)
            Console.WriteLine($"Log level: {level}, Message: {s}");
    };

    await ftp.ConnectAsync(cancellationToken);

    return ftp;
}